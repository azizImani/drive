## Steps to follow

You’ll start by unzipping this file, then follow the hereunder

1. Packaging the project into a web application archive using **mvn compile war:exploded**
2. Add the web application to the Context container of tomcat, by editing tomcat_folder/bin/server.xml
   and adding < Context docBase="path_to_project\drive\target\drive-model-1.0-SNAPSHOT" path="your_path" reloadable="true" backgroundProcessorDelay="4" /> 
3. Run tomcat server by running **catalina.bat run** under tomcat_folder/bin/
4. Import initial data to your database using /init action, for example: localhost:8181/drive/app/init
   (please change 8181 by your tomcat port and drive by the path you already put in tomcat server.xml)

You can change the database server to mysql by changing the persistence.xml file (uncomment mysql and comment h2).

FYI there is an already deployed version of this project, you can find it here: https://stark-atoll-43207.herokuapp.com/app/
Test Credentials : jdoe/pass

Thanks and Regards.