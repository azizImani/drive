package fr.eservices.drive.models;


import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="Article_Type")
/* using @Data annotation to generate getters, setters, toString, equals and hashCode */
@Data
public abstract class Article {
    @Id
    private int id;
    private String ean;
    private int price;
    private double vat;
    private String name;
    private String image;

    @ManyToMany
    private List<Category> categories = new ArrayList<>();

}
