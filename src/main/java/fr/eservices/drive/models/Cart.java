package fr.eservices.drive.models;

import fr.eservices.drive.dao.CartEntryRepository;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.FetchType.EAGER;

@Entity
@Data
public class Cart {
    @Id
    @GeneratedValue
    private int id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public Cart() {
        createdOn = new Date();
    }

    // changing from list of article to list of cart entry (the original model doesn't support buying article by specifying a quantity)
    @OneToMany()
    private List<CartEntry> cartEntries  = new ArrayList<>();
    //private List<Article> articles;

    @ManyToOne(fetch = EAGER)
    private Customer customer;

    private final static int NOT_FOUND = -1;

    public void add(CartEntry newCartEntry, CartEntryRepository cartEntryRepos) {
        int index = findEntryByArticle(newCartEntry.getArticle());
        if (index != NOT_FOUND) {
            CartEntry oldEntry = getCartEntries().get(index);
            updateEntryQuantity(oldEntry, newCartEntry);
            cartEntryRepos.updateQuantity(newCartEntry.getQuantity(), oldEntry.getId());
        }else{
            getCartEntries().add(newCartEntry);
            cartEntryRepos.save(newCartEntry);
        }
    }

    private void updateEntryQuantity(CartEntry oldEntry, CartEntry newCartEntry) {
        int existedQuantity = oldEntry.getQuantity();
        newCartEntry.setQuantity(existedQuantity + newCartEntry.getQuantity());
    }

    public int count() {
        int sum = 0;
        for (CartEntry entry : getCartEntries()) {
            sum += entry.getQuantity();
        }
        return sum;
    }

    public List<CartEntry> getCartEntries() {
        return cartEntries;
    }

    public void remove(Article article) {
        int index = findEntryByArticle(article);
        if (index != NOT_FOUND) {
            getCartEntries().remove(index);
        }
    }

    private int findEntryByArticle(Article article) {
        for (int i = 0; i < getCartEntries().size(); i++) {
            Article current = getCartEntries().get(i).getArticle();
            if (current.getId() == article.getId()) {
                return i;
            }
        }
        return NOT_FOUND;
    }

    public void clear() {
        cartEntries.clear();
    }

}
