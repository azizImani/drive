package fr.eservices.drive.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.FetchType.EAGER;

@Entity
@Data
public class Category {
    @Id
    private int id;
    private String name;
    private int orderIdx;

    @ManyToMany(fetch = EAGER, mappedBy="categories")
    private List<Article> articles = new ArrayList<>();

}
