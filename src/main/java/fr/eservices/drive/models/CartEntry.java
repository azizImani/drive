package fr.eservices.drive.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
public class CartEntry {
    @Id @GeneratedValue
    private int id;
    private int quantity;

    @OneToOne
    private Article article;
}
