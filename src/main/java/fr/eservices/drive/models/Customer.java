package fr.eservices.drive.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
@Data
public class Customer {
    @Id @GeneratedValue
    private int id;
    private String login;
    private String firstName;
    private String lastName;
    private String email;
    private String password;

    @OneToOne
    private Cart activeCart;


    public Cart getActiveCart() {
        if(activeCart == null){
            activeCart = new Cart();
        }
        return activeCart;
    }
}
