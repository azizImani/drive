package fr.eservices.drive.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

import javax.persistence.*;

@Entity
@Data
public class StatusHistory {
    @Id @GeneratedValue
    private int id;
    @Temporal(TemporalType.TIME)
    private Date statusDate;
    @Column(name = "history_status")
    private Status status;
}
