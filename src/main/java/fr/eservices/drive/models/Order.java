package fr.eservices.drive.models;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "orders")
@Data
public class Order {
    @Id @GeneratedValue
    private int id;
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;
    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryDate;
    private double amount;

    @OneToMany
    private List<StatusHistory> history = new ArrayList<>();

    private Status currentStatus;

    @ManyToMany
    private List<Article> articles;

    @ManyToOne
    private Customer customer;

}
