package fr.eservices.drive.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class StockEntry {

    @Id @GeneratedValue
    private int id;
    @OneToOne
    private Article article;
    private int quantity;

    public void reduceQuantity(int quantity){
        this.quantity -= quantity;
    }
}
