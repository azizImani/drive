package fr.eservices.drive.services;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import fr.eservices.drive.web.dto.Article;
import fr.eservices.drive.web.dto.Category;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class CsvReader {

    private static final String CATEGORY_FILE = "/data/categories.csv";
    private static final String ARTICLES_FILE = "/data/articles.csv";

    public static <T> List<T> load(Class<T> type, String dataFile) {
        List<T> list = new ArrayList<T>();
        try {
            CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
            CsvMapper mapper = new CsvMapper();
            File file = new ClassPathResource(dataFile).getFile();
            MappingIterator<T> readValues =
                    mapper.readerFor(type)
                            .with(bootstrapSchema)
                            .readValues( new InputStreamReader(new FileInputStream(file),"UTF-8") );
            while(readValues.hasNext()){
                T entry = null;
                try {
                    entry = readValues.next();
                    list.add(entry);
                }catch (Exception e){
                    System.out.println(entry);
                }

            }
            //return readValues.readAll();
            return list;

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error occurred while loading category list from file " + dataFile);
            return Collections.emptyList();
        }
    }
    public CsvReader(){

    }

    public List<Category> categories(){
        return load(Category.class, CATEGORY_FILE);
    }

    public List<Article> articles(){
        return load(Article.class, ARTICLES_FILE);
    }

}
