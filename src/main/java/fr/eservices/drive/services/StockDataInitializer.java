package fr.eservices.drive.services;


import fr.eservices.drive.dao.ArticleRepository;
import fr.eservices.drive.dao.StockRepository;
import fr.eservices.drive.models.Article;
import fr.eservices.drive.models.StockEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class StockDataInitializer implements Initializer{

    @Autowired
    private ArticleRepository articleRepo;
    @Autowired
    private StockRepository stockRepo;

    public void init(){
        articleRepo.findAll().forEach(article -> {
            StockEntry stockEntry = new StockEntry();
            stockEntry.setArticle(article);
            stockEntry.setQuantity(70);
            stockRepo.save(stockEntry);
        });
    }

}
