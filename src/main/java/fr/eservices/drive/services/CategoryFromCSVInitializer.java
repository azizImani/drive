package fr.eservices.drive.services;

import fr.eservices.drive.dao.CategoryRepository;
import fr.eservices.drive.models.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CategoryFromCSVInitializer implements Initializer{

    @Autowired
    private CategoryRepository categoryRepo;
    @Autowired
    private CsvReader csvReader;
    public void init(){
        List<fr.eservices.drive.web.dto.Category> categories = csvReader.categories();
        categories.forEach(categoryDTO -> {
            Category category = new Category();
            category.setId(Integer.parseInt(categoryDTO.getId()));
            category.setName(categoryDTO.getName());
            category.setOrderIdx(Integer.parseInt(categoryDTO.getOrder()));
            categoryRepo.save(category);
        });
    }
}
