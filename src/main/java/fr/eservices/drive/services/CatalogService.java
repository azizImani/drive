package fr.eservices.drive.services;

import fr.eservices.drive.models.Article;
import fr.eservices.drive.models.Category;
import fr.eservices.drive.models.Perishable;

import java.util.Date;
import java.util.List;

public interface CatalogService {

    /**
     * List categories, ordered by order_idx
     *
     * @return list of all categories
     */
    List<Category> getCategories();

    /**
     * List categories associated to an article
     *
     * @return categories of article, can be empty
     * @throws DataException if article does not exist
     */
    List<Category> getArticleCategories(int articleId);

    /**
     * List all articles in a category
     *
     * @return articles, can be empty
     * @throws DataException if category does not exist
     */
    List<Article> getCategoryContent(int categoryId);

    /**
     * List perished article considering a defined day.
     *
     * @param day for perishable date to compare with
     * @return perished article, can be empty
     * @throws DataException if more than 200 perishable exits
     */
    List<Perishable> getPerished(Date day);


    boolean isInStock(Article article);

}
