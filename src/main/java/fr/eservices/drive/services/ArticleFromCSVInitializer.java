package fr.eservices.drive.services;

import fr.eservices.drive.dao.ArticleRepository;
import fr.eservices.drive.dao.CategoryRepository;
import fr.eservices.drive.models.Article;
import fr.eservices.drive.models.Category;
import fr.eservices.drive.models.Perishable;
import fr.eservices.drive.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class ArticleFromCSVInitializer implements Initializer {

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ArticleRepository articleRepository;
    @Autowired
    private CsvReader csvReader;
    public void init() {
        List<fr.eservices.drive.web.dto.Article> articles = csvReader.articles();
        articles.forEach(articleDTO -> {
            Article article = new Product();
            article.setId(Integer.parseInt(articleDTO.getId()));
            article.setName(articleDTO.getName());
            article.setPrice(Integer.parseInt(articleDTO.getPrice()));
            article.setImage(articleDTO.getImg());
            Category articleCategory = categoryRepository.findOne(Integer.parseInt(articleDTO.getCat_id()));
            article.getCategories().add(articleCategory);
            articleRepository.save(article);
        });
    }
}
