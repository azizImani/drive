package fr.eservices.drive.services;

import fr.eservices.drive.dao.ArticleRepository;
import fr.eservices.drive.dao.CategoryRepository;
import fr.eservices.drive.models.Article;
import fr.eservices.drive.models.Category;
import fr.eservices.drive.models.Perishable;
import fr.eservices.drive.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ArticleDataInitializer implements Initializer{

    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private ArticleRepository articleRepository;

    public void init() {
        {
            Article article = new Product();
            article.setId(10101010);
            article.setName("Boisson energetique");
            article.setPrice(299);
            article.setImage("https://static1.chronodrive.com/img/PM/P/0/76/0P_61276.gif");
            Category boissons = categoryRepository.findOne(1);
            article.getCategories().add(boissons);
            articleRepository.save(article);
        }

        {
            Perishable article = new Perishable();
            article.setId(10101012);
            article.setName("Papier Cadeau");
            article.setPrice(150);
            article.setImage("https://static1.chronodrive.com/img/PM/P/0/72/0P_348972.gif");
            article.getCategories().add(categoryRepository.findOne(6));
            article.setBestBefore(new Date());
            article.setLot("lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor");
            articleRepository.save(article);
        }

        {
            Article article = new Product();
            article.setId(10101013);
            article.setName("Pur jus d'orange");
            article.setPrice(235);
            article.setImage("https://static1.chronodrive.com/img/PM/P/0/42/0P_40042.gif");
            article.getCategories().add(categoryRepository.findOne(1));
            articleRepository.save(article);
        }

        {
            Article article = new Product();
            article.setId(195420);
            article.setName("420g Fromage a raclette");
            article.setPrice(450);
            article.setImage("https://static1.chronodrive.com/img/PM/P/0/20/0P_195420.gif");
            article.getCategories().add(categoryRepository.findOne(5));
            articleRepository.save(article);
        }

        {
            Article article = new Product();
            article.setId(125482);
            article.setName("BOEUF : 2 Bavettes d'Aloyau");
            article.setPrice(599);
            //article.setImage("https://static1.chronodrive.com/img/PM/V/0/82/0V_125482.gif");
            article.setImage("https://static1.chronodrive.com/img/PM/P/0/82/0P_125482.gif");
            article.getCategories().add(categoryRepository.findOne(2));
            articleRepository.save(article);
        }

        {
            Article article = new Product();
            article.setId(120574);
            article.setName("2,5 kg Pomme de terre Cat 1");
            article.setPrice(169);
            article.setImage("https://static1.chronodrive.com/img/PM/P/0/74/0P_120574.gif");
            article.getCategories().add(categoryRepository.findOne(4));
            articleRepository.save(article);
        }

        {
            Article article = new Product();
            article.setId(129266);
            article.setName("AUCHAN : 6 merguez boeuf mouton");
            article.setPrice(329);
            //article.setImage("https://static1.chronodrive.com/img/PM/V/0/66/0V_129266.gif");
            article.setImage("https://static1.chronodrive.com/img/PM/P/0/66/0P_129266.gif");
            article.getCategories().add(categoryRepository.findOne(2));
            article.getCategories().add(categoryRepository.findOne(3));
            articleRepository.save(article);
        }

    }
}
