package fr.eservices.drive.dao;

import fr.eservices.drive.models.Article;
import fr.eservices.drive.models.StockEntry;
import org.springframework.data.repository.CrudRepository;

public interface StockRepository extends CrudRepository<StockEntry, Integer> {
    StockEntry findByArticle(Article article);
}
