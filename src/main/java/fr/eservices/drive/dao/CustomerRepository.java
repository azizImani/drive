package fr.eservices.drive.dao;

import fr.eservices.drive.models.Customer;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    Customer findByLoginAndPassword(String login, String password);

    Customer findByLogin(String login);
}
