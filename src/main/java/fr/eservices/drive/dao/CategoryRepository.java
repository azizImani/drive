package fr.eservices.drive.dao;

import fr.eservices.drive.models.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
    List<Category> findAllByOrderByOrderIdxAsc();
}
