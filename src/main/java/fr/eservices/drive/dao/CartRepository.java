package fr.eservices.drive.dao;


import fr.eservices.drive.models.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Integer>{
}
