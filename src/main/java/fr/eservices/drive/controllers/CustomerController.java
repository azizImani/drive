package fr.eservices.drive.controllers;

import fr.eservices.drive.models.Customer;
import fr.eservices.drive.web.dto.CustomerForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static fr.eservices.drive.controllers.Paths.AppPaths.APP_URL_PATTERN;
import static fr.eservices.drive.controllers.Paths.CartPaths.CART_READ_ENDPOINT;
import static fr.eservices.drive.controllers.Paths.CustomerPaths.*;
import static fr.eservices.drive.controllers.Paths.OrderPaths.ORDER_CONFIRM_ENDPOINT;

@Controller
public class CustomerController extends AbstractController {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @GetMapping(path = {"/login", "login"})
    public String showLogin(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse response) {
        if (isCustomerAuthenticatedIn(session)) {
            return "index";
        }
        model.addAttribute(REFERER_HEADER, request.getHeader(REFERER_HEADER));
        populateHeaderData(model, request);
        return "login";
    }

    @PostMapping(path = LOGIN_ENDPOINT)
    public String login(Model model, HttpSession session, HttpServletRequest request, HttpServletResponse RESPONSE) {
        String login = request.getParameter("login");
        String password = request.getParameter("password");
        Customer customer = customerRepo.findByLogin(login);

        if (!userIsLoggedIn(password, customer)) {
            model.addAttribute("loginError", "login or password invalid!");
            return "login";
        }
        session.setAttribute(CUSTOMER_SESSION_ID, customer.getId());
        if (shouldRedirectToOrderPage(request)) {
            return "redirect:" + APP_URL_PATTERN + "/" + ORDER_CONFIRM_ENDPOINT;
        }
        return "redirect:home";
    }

    private boolean userIsLoggedIn(String password, Customer customer) {
        if(customer == null){
            return false;
        }
        return passwordEncoder.matches(password, customer.getPassword());
    }

    private boolean shouldRedirectToOrderPage(HttpServletRequest request) {
        return request.getParameter(REFERER_HEADER.toLowerCase()).contains(CART_READ_ENDPOINT);
    }

    @GetMapping(path = REGISTRATION_ENDPOINT)
    public String register(Model model, HttpSession session, HttpServletRequest request) {
        if (isCustomerAuthenticatedIn(session)) {
            return "index";
        }
        populateHeaderData(model, request);
        model.addAttribute("customerForm", new CustomerForm());
        return "register-user";
    }

    @PostMapping(path = REGISTRATION_ENDPOINT)
    public String register(@Valid CustomerForm customerForm, BindingResult bindingResult, HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "register-user";
        }
        Customer customer = customerForm.toCustomerModel(passwordEncoder);
        customerRepo.save(customer);
        authenticate(session, customerForm);
        return "redirect:home";
    }

    @GetMapping(path = LOGOUT_ENDPOINT)
    public String logout(HttpServletResponse response, HttpSession session) {
        session.invalidate();
        response.addCookie(invalidateCartCookie());
        return "redirect:home";
    }

    private void authenticate(HttpSession session, CustomerForm customerForm) {
        int customerId = customerRepo.findByLogin(customerForm.getLogin()).getId();
        session.setAttribute(CUSTOMER_SESSION_ID, customerId);
    }
}
