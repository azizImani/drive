package fr.eservices.drive.controllers;

import fr.eservices.drive.dao.ArticleRepository;
import fr.eservices.drive.dao.CartEntryRepository;
import fr.eservices.drive.models.Article;
import fr.eservices.drive.models.Cart;
import fr.eservices.drive.models.CartEntry;
import fr.eservices.drive.services.TotalAmountCalculator;
import fr.eservices.drive.web.dto.CartEntryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import static fr.eservices.drive.controllers.Paths.CartPaths.*;
import static fr.eservices.drive.web.dto.SimpleResponse.Status.ERROR;
import static fr.eservices.drive.web.dto.SimpleResponse.Status.OK;

@Controller
public class CartController extends AbstractController {

    @Autowired
    private ArticleRepository articleRepo;
    @Autowired
    private TotalAmountCalculator amountCalculator;
    @Autowired
    private CartEntryRepository cartEntryRepos;


    @GetMapping(path = CART_READ_ENDPOINT)
    public String cart(Model model, HttpServletRequest request) {
        Cart activeCart = getCartFrom(request);
        activeCart = activeCart != null ? activeCart : new Cart();
        populateHeaderData(model, activeCart);
        List<CartEntry> cartEntries = activeCart.getCartEntries();
        model.addAttribute("cartEntries", cartEntries);
        return "cart";
    }

    @GetMapping(path = CART_CLEAR_ENDPOINT)
    public String clearCart(Model model, HttpServletRequest request) {
        Cart activeCart = getCartFrom(request);
        activeCart.clear();
        populateHeaderData(model, activeCart);
        return "index";
    }

    @PostMapping(path = CART_ADD_ENDPOINT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> addToCart(@RequestBody CartEntryDTO cartEntryDTO, HttpServletRequest request) {
        Cart activeCart = getCartFrom(request);
        CartEntry cartEntry = toCartEntry(cartEntryDTO);
        return updateCart(activeCart, cartEntry.getArticle(), cart -> cart.add(cartEntry, cartEntryRepos));
    }

    @GetMapping(path = CART_REMOVE_ENDPOINT)
    @ResponseBody
    public Map<String, Object> removeFromCart(@PathVariable int productId, HttpServletRequest request) {
        Article article = articleRepo.findOne(productId);
        Cart activeCart = getCartFrom(request);
        return updateCart(activeCart, article, cart -> cart.remove(article));
    }

    private Map<String, Object> updateCart(Cart activeCart, Article article, Consumer<Cart> action) {
        Map<String, Object> response = new HashMap<>();
        if (article == null) {
            response.put("status", ERROR);
            return response;
        }
        action.accept(activeCart);
        cartRepository.save(activeCart);
        response.put("count", activeCart.count());
        response.put("total", amountCalculator.totalPrices(activeCart));
        response.put("status", OK);
        return response;
    }

    // Creation of cart entity from cartDTO
    private CartEntry toCartEntry(CartEntryDTO cartEntryDTO) {
        Article article = articleRepo.findOne(cartEntryDTO.getArticleId());
        CartEntry cartEntry = new CartEntry();
        cartEntry.setArticle(article);
        cartEntry.setQuantity(cartEntryDTO.getQuantity());
        return cartEntry;
    }

}
