package fr.eservices.drive.controllers;

import fr.eservices.drive.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import static fr.eservices.drive.controllers.Paths.AppPaths.APP_INIT;

@Controller
public class InitController {

    @Autowired
    private ArticleDataInitializer articlesInitializer;
    @Autowired
    private CategoryDataInitializer categoriesInitializer;


    @Autowired
    private ArticleFromCSVInitializer articleFromCSV;
    @Autowired
    private CategoryFromCSVInitializer categoryFromCSV;


    @Autowired
    private StockDataInitializer stockInitializer;
    @Autowired
    private CustomerInitializer customerInitializer;

    @GetMapping(path = APP_INIT)
    @ResponseBody
    public String init(){
        // Csv initializer has more data, so activating the the default initializer for local test
        //categoryFromCSV.init();
        //articleFromCSV.init();
        categoriesInitializer.init();
        articlesInitializer.init();
        stockInitializer.init();
        customerInitializer.init();
        return "Initialization succeeded!";
    }
}
