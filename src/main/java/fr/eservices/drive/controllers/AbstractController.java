package fr.eservices.drive.controllers;

import fr.eservices.drive.dao.CartRepository;
import fr.eservices.drive.dao.CustomerRepository;
import fr.eservices.drive.models.Cart;
import fr.eservices.drive.models.Customer;
import fr.eservices.drive.services.CatalogService;
import fr.eservices.drive.services.TotalAmountCalculator;
import fr.eservices.drive.web.dto.HeaderDTOBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static fr.eservices.drive.controllers.Paths.AppPaths.APP_URL_PATTERN;

public abstract class AbstractController {

    protected static final String CUSTOMER_SESSION_ID = "auth";
    protected static final String SHOOPY_CART_ID = "SHOOPY_CART";

    protected static final String REFERER_HEADER = "Referer";

    @Autowired
    protected CatalogService catalogService;

    @Autowired
    protected TotalAmountCalculator amountCalculator;

    @Autowired
    protected CustomerRepository customerRepo;
    
    @Autowired
    protected CartRepository cartRepository;


    //Insert necessary data to the header tag
    public void populateHeaderData(Model model, Cart activeCart) {
        HeaderDTOBuilder.HeaderDTO headerDTO = new HeaderDTOBuilder().
                withAmountCalculator(amountCalculator)
                .withActiveCart(activeCart)
                .withCategories(catalogService.getCategories()).build();
        model.addAttribute("headerDTO", headerDTO);
    }


    public void populateHeaderData(Model model, HttpServletRequest request) {
        Cart activeCart = getCartFrom(request);
        if(activeCart == null){
            activeCart = new Cart();
        }
        populateHeaderData(model, activeCart);
    }

    protected Customer getAuthenticatedCustomer(HttpSession session) {
        Integer customerId = (Integer) session.getAttribute(CUSTOMER_SESSION_ID);
        if (customerId == null) {
            return null;
        }
        return customerRepo.findOne(customerId);
    }

    protected boolean isCustomerAuthenticatedIn(HttpSession session) {
        return session.getAttribute(CUSTOMER_SESSION_ID) != null;
    }

    protected Cart getCartFrom(HttpServletRequest request){
        Cookie cookie = getCartCookieFrom(request);
        if(cookie == null){
            return null;
        }
        return getCartFrom(cookie);
    }

    /* I'm based on cookies instead of http session, because in good practice
        REST api shouldn't be aware of app context (REST is stateless)
     */
    protected Cookie createCartCookie(HttpServletRequest request) {
        Cookie cookie = getCartCookieFrom(request);
        if (expiredCart(cookie)) {
            int id = generateAnonymousCart();
            cookie = new Cookie(SHOOPY_CART_ID, String.valueOf(id));
            cookie.setHttpOnly(true);
            cookie.setPath(getPath(request));
        }
        return cookie;
    }

    protected boolean expiredCartCookieFrom(HttpServletRequest request){
        Cookie cookie = getCartCookieFrom(request);
        return expiredCart(cookie);
    }

    private String getPath(HttpServletRequest request) {
        return request.getContextPath() + APP_URL_PATTERN;
    }

    protected Cookie invalidateCartCookie() {
        Cookie cookie = new Cookie(SHOOPY_CART_ID, null); // Not necessary, but saves bandwidth.
        cookie.setHttpOnly(true);
        cookie.setMaxAge(0);
        return cookie;
    }

    private boolean expiredCart(Cookie cookie){
        if(cookie == null || getCartFrom(cookie) == null){
            return true;
        }
        return false;
    }

    private Cookie getCartCookieFrom(HttpServletRequest request) {
        if(request.getCookies() == null){
            return null;
        }
        for (Cookie cookie : request.getCookies()) {
            if (cookie.getName().equals(SHOOPY_CART_ID)) {
                return cookie;
            }
        }
        return null;
    }

    // Regarding the above comment, we save the cart in the database instead of session
    private int generateAnonymousCart() {
        Cart cart = new Cart();
        cart = cartRepository.save(cart);
        return cart.getId();
    }

    private Cart getCartFrom(Cookie cookie) {
        int cartId = cartIdFrom(cookie);
        return cartRepository.findOne(cartId);
    }

    private int cartIdFrom(Cookie cookie) {
        String cookieValue = cookie.getValue();
        return Integer.parseInt(cookieValue);
    }

}
