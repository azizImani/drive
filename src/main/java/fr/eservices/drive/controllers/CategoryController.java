package fr.eservices.drive.controllers;


import fr.eservices.drive.dao.CategoryRepository;
import fr.eservices.drive.models.Article;
import fr.eservices.drive.models.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static fr.eservices.drive.controllers.Paths.CategoryPaths.CATEGORY_GET_CONTENT_ENDPOINT;

@Controller
public class CategoryController extends AbstractController{

    @Autowired
    private CategoryRepository categoryRepo;

    @GetMapping(path = CATEGORY_GET_CONTENT_ENDPOINT)
    public String products(@PathVariable int id, Model model, HttpServletRequest request, HttpServletResponse response){
        if(expiredCartCookieFrom(request)){
            Cookie cookie = createCartCookie(request);
            response.addCookie(cookie);
        }
        populateHeaderData(model, request);
        Category category = categoryRepo.findOne(id);
        List<Article> articles = getInStockArticlesFrom(category);
        model.addAttribute("products", articles);
        model.addAttribute("category", category);
        return "articles";
    }

    private List<Article> getInStockArticlesFrom(Category category){
        List<Article> articles = category.getArticles();
        return articles.stream().filter(isInStock()).collect(Collectors.toList());
    }

    private Predicate<Article> isInStock(){
        return article -> catalogService.isInStock(article);
    }

}
