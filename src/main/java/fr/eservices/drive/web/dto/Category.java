package fr.eservices.drive.web.dto;

import lombok.Data;

@Data
public class Category {

    private String id;
    private String name;
    private String order;

}
