package fr.eservices.drive.web.dto;

import lombok.Data;

@Data
public class Article {
	
	private String id;
	private String price;
	private String name;
	private String img;
	private String cat_id;
	
}
