package fr.eservices.drive.web.dto;

import fr.eservices.drive.models.Customer;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.springframework.security.crypto.password.PasswordEncoder;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CustomerForm {
    @NotNull
    private String login;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    @Email
    private String email;
    @NotNull
    @Size(min = 8,max = 32)
    private String password;

    public CustomerForm() {
    }

    public Customer toCustomerModel(PasswordEncoder passwordEncoder){
        Customer customer = new Customer();
        customer.setEmail(email);
        customer.setFirstName(firstName);
        customer.setLogin(login);
        customer.setPassword(passwordEncoder.encode(password));
        customer.setLastName(lastName);
        return customer;
    }
}

