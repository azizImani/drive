package fr.eservices.drive.web.dto;

import lombok.Data;

@Data
public class CartEntryDTO {
    private int articleId;
    private int quantity;

    public CartEntryDTO(){

    }
}
