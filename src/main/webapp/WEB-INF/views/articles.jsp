<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="tag" %>
<% String ctxPath = request.getContextPath(); %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Ecommerce Website</title>

    <!-- Bootstrap Core CSS -->
    <link href="<%= ctxPath %>/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<%= ctxPath %>/css/style.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<tag:header headerData="${headerDTO}"/>

<!-- Page Content -->
<div class="container">

    <!-- Title -->
    <div class="row">
        <div class="col-lg-12">
            <h3>${category.name}</h3>
        </div>
    </div>
    <!-- /.row -->

    <!-- Page Features -->
    <div class="row text-center">
        <c:forEach items="${products}" var="product" varStatus="loop">
        <c:if test="${loop.index % 4 == 0}">
    </div>
    <div class="row text-center">
        </c:if>
        <div class="col-md-3 col-sm-6 hero-feature">
            <div class="thumbnail">
                <img src="${product.image}" alt="">
                <div class="caption">
                    <h3>${product.name}</h3>
                    <p>Description: Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
                    <p>Price: ${product.price} c</p>
                    <div class="input-group">
                        <span class="input-group-addon" id="basic-addon1">Quantity</span>
                        <input type="text" id="${product.id}" class="form-control" value="1"
                               aria-describedby="basic-addon1">
                    </div>
                    <p>
                        <a href="<%= ctxPath %>/app/cart/add/" class="btn btn-primary update-cart buy-btn">Buy Now!</a>
                        <a href="#"></a>
                    </p>
                </div>
            </div>
        </div>

        </c:forEach>


    </div>
    <!-- /.row -->

    <hr>
    <tag:footer/>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<script type="text/javascript" src="<%= ctxPath %>/js/cart.js"></script>

</body>

</html>
